<?php
    $bdd = new PDO('mysql:dbname=colyseum;host=localhost','root','');
    
   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD</title>
</head>
<body>

<h1>Exercice 1</h1>
<?php
$reponse1 = $bdd->query("SELECT * FROM clients");
while($donnees = $reponse1->fetch()){
    echo '<u><i>' . $donnees[lastName] .  " " . $donnees[firstName] . '</i><br></u>';
 }
?>

<h1>Exercie 2</h1>
<?php
$reponse2 = $bdd->query("SELECT * FROM showTypes");
 while($donnees = $reponse2->fetch()){
    echo '<u><i>' . $donnees[type] . '</i><br></u>';
 }
?>

<h1>Exercie 3 </h1>
<?php
$reponse3 = $bdd->query("SELECT * FROM clients ORDER BY id ASC LIMIT 20");

 while($donnees = $reponse3->fetch()){
    echo '<u><i>' . $donnees[lastName] .  " " . $donnees[firstName] . '</i><br></u>';
 }
?>

<h1> Exercice 4 </h1>
<?php
$reponse4 = $bdd->query("SELECT * FROM clients WHERE card = 1");
 while($donnees = $reponse4->fetch()){
    echo '<u><i>' . $donnees[lastName] .  " " . $donnees[firstName] . '</i><br></u>';
 }
?>

<h1>Exercice 5</h1>
<?php
$reponse5 = $bdd->query("SELECT * FROM clients WHERE lastName  LIKE 'M%' ORDER BY lastName ");
 while($donnees = $reponse5->fetch()){
    echo '<u><i>' . $donnees[lastName] .  " " . $donnees[firstName] .'</i><br></u>';
 }
?>

<h1> Exercice 6</h1>
<?php
$reponse6 = $bdd->query("SELECT * FROM shows ORDER BY title");
 while($donnees = $reponse6->fetch()){
    echo '<u><i>' . $donnees[title] .  " par " . $donnees[performer] . " le " . $donnees[date] . " à " . $donnees[startTime] . '</i><br></u>';
 }
?>

<h1>Exercie 7 </h1>
<?php
$reponse7 = $bdd->query("SELECT * FROM clients; ");
 while($donnees = $reponse7->fetch()){
     if($donnees[card] === '1') 
     {
         $donnees[card] = "oui";
     }
     else
     {
         $donnees[card] = "non";
     }
    echo '<u><i>' . $donnees[lastName] . " " . $donnees[firstName] . " " . $donnees[card] . "  " . $donnees[cardNumber] . '</i><br></u>';
 }
?>
<!-- "SELECT * , CASE WHEN card = 1 THEN 'oui' WHEN card = 0 THEN 'non' ELSE 'Etat inconnu' END FROM clients;  -->
    
</body>
</html> 
